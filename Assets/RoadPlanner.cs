﻿using HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RoadPlanner : Singleton<RoadPlanner>, IInputClickHandler {
    public GameObject pieceOfRoadPrefab;
    public GameObject waypointPrefab;

    private GameObject newRoadStart = null;
    private PieceOfRoad newRoad = null;

    public void StartNewTrackAt(GameObject obj)
    {
        newRoadStart = obj;
        GameObject road = GameObject.Instantiate(pieceOfRoadPrefab);
        newRoad = road.GetComponent<PieceOfRoad>();
        InputManager.Instance.OverrideFocusedObject = gameObject;
    }

    private void Update()
    {
        if (newRoad != null)
        {
            RaycastHit hitInfo;
            if (Physics.Raycast(newRoadStart.transform.position + Vector3.up * 0.05f, GazeManager.Instance.HitPosition - newRoadStart.transform.position - Vector3.up * 0.05f, out hitInfo, (GazeManager.Instance.HitPosition - newRoadStart.transform.position - Vector3.up * 0.05f).magnitude + 0.3f))
            {
                transform.position = hitInfo.point;
            }
            else
            {
                transform.position = GazeManager.Instance.HitPosition;
            }

            newRoad.setPoints(newRoadStart, this.gameObject);
        }
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {
        if (newRoad != null)
        {
            GameObject gazeObj = null;
            Vector3 gazePoint;
            RaycastHit hitInfo;
            if (Physics.Raycast(newRoadStart.transform.position + Vector3.up * 0.05f, GazeManager.Instance.HitPosition - newRoadStart.transform.position - Vector3.up * 0.05f, out hitInfo, (GazeManager.Instance.HitPosition - newRoadStart.transform.position - Vector3.up * 0.05f).magnitude + 0.3f))
            {
                gazeObj = hitInfo.transform.gameObject;
                gazePoint = hitInfo.point;
            }
            else
            {
                gazePoint = GazeManager.Instance.HitPosition;
            }
            transform.position = gazePoint;
            bool canConnect = true;
            if (gazeObj == null || gazeObj.GetComponentInParent<RoadPoint>() == null)
            {
               // if (Headquarters.Instance.TakeMoney(10 * Vector3.Distance(transform.position, newRoadStart.transform.position))){
                    gazeObj = Instantiate(waypointPrefab, this.transform.position, Quaternion.identity);
                    canConnect = true;
                //}
                //Manager.Instance.addWaypoint(gazeObj);
            }

            if (canConnect)
            {
                gazeObj = gazeObj.GetComponentInParent<RoadPoint>().gameObject;
                newRoad.setPoints(newRoadStart, gazeObj);
                Manager.Instance.addConnection(newRoad.gameObject);
            }
            newRoad = null;
            newRoadStart = null;

            if (InputManager.Instance.OverrideFocusedObject == gameObject)
            {
                InputManager.Instance.OverrideFocusedObject = null;
            }
        }
    }
}
