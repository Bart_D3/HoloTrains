﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ore : MonoBehaviour {
    public enum Type
    {
        COAL,
        IRON,
        COPPER,
        ROCKETPART
    };

    public Type oreType;

    public Material coalMaterial;
    public Material ironMaterial;
    public Material copperMaterial;
    public GameObject prefabCoalMine;
    public GameObject prefabIronMine;
    public GameObject prefabCopperMine;

    public string OreName
    {
        get
        {
            switch (oreType)
            {
                case Type.COAL:
                    return "Carbon ore";
                case Type.IRON:
                    return "Iron ore";
                case Type.COPPER:
                    return "Copper ore";
                default:
                    return "Ore?";
            }
        }
    }
    
    void Start() {
        oreType = (Type)Random.Range(0, 3);
        gameObject.name = OreName;

        Material mat = null;
        switch (oreType)
        {
            case Type.COAL:
                mat = coalMaterial;
                break;
            case Type.IRON:
                mat = ironMaterial;
                break;
            case Type.COPPER:
                mat = copperMaterial;
                break;
        }

        GetComponentInChildren<MeshRenderer>().material = mat;
    }

    public void BuildCave()
    {
		if (GetComponentInChildren<Cave> ()) {
			StatusText.Instance.Text = "The mine already exists";
		} else {
			StatusText.Instance.Text = "You built a(n)" + gameObject.name + "mine";

			int amount = 99999999;
			switch (oreType) {
			case Ore.Type.COAL:
				amount = 10;
				break;
			case Ore.Type.COPPER:
				amount = 20;
				break;
			case Ore.Type.IRON:
				amount = 30;
				break;
			}

			if (!Headquarters.Instance.TakeMoney (amount)) {
				StatusText.Instance.Text = "You don't have enough money, you need to have $" + amount;
				return;
			}

			GameObject prefab = null;
			switch (oreType) {
			case Type.COAL:
				prefab = prefabCoalMine;
				break;
			case Type.IRON:
				prefab = prefabIronMine;
				break;
			case Type.COPPER:
				prefab = prefabCopperMine;
				break;
			}
			GameObject obj = GameObject.Instantiate (prefab);
			obj.transform.parent = transform;
			obj.transform.localScale = new Vector3 (0.1f, 0.1f, 0.1f);
			obj.transform.localPosition = new Vector3 (0.0f, 0.0775f, 0.0f);
			obj.transform.localRotation = Quaternion.identity;
		}

    }
}
