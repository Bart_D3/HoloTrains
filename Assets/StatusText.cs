﻿using HoloToolkit.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusText : Singleton<StatusText> {
    public string Text {
        get
        {
            return GetComponent<TextMesh>().text;
        }
        set
        {
            GetComponent<TextMesh>().text = value;
        }
    }

    public void MoveTo(Vector3 pos)
    {
        GetComponent<Interpolator>().SetTargetPosition(pos);
    }

    public void MoveToAbove(GameObject obj)
    {
        Bounds combinedBounds = new Bounds(obj.transform.position, Vector3.zero);
        foreach (Renderer render in obj.GetComponentsInChildren<Renderer>())
        {
            if (render.bounds.extents == Vector3.zero)
                continue;
            combinedBounds.Encapsulate(render.bounds);
        }
        MoveTo(new Vector3(
            combinedBounds.center.x,
            combinedBounds.max.y + 0.1f,
            combinedBounds.center.z
        ));
    }
}
