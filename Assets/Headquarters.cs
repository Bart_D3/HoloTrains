﻿using HoloToolkit.Unity;
using HoloToolkit.Unity.SpatialMapping;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(TapToPlace))]
[RequireComponent(typeof(HeadquatersBuilding))]
public class Headquarters : Singleton<Headquarters> {
    public GameObject warehousePrefab;
    public GameObject factoryPrefab;
    private float money = 1000; //20;

    void Start()
    {
        gameObject.name = "Headquarters";
    }

    GameObject newBuilding = null;

    public void CreateBuilding(GameObject prefab, int cost)
    {
        if (!TakeMoney(cost))
        {
            StatusText.Instance.Text = "You don't have enough money, you need to have $" + cost;
        }
        newBuilding = GameObject.Instantiate(prefab);
        TapToPlace placer = newBuilding.AddComponent<TapToPlace>();
        placer.IsBeingPlaced = true;
        StatusText.Instance.Text = "Tap to put it";
    }

    void Update()
    {
        if (newBuilding != null)
        {
            TapToPlace tapToPlace = newBuilding.GetComponent<TapToPlace>();
            if (tapToPlace != null && tapToPlace.enabled)
            {
                if (!tapToPlace.IsBeingPlaced)
                {
                    tapToPlace.enabled = false;
                    newBuilding = null;
                }
            }
        }
        
        string text = "Money: $" + money + "\n";
        foreach (Item item in GetComponent<HeadquatersBuilding>().getItems())
        {
            text += "\n"+item.ToString();
        }
        Debug.Log(text);
        GetComponent<ObjectWithUI>().Text = text;
    }

    public void CreateWarehouse()
    {
        CreateBuilding(warehousePrefab, 10);
    }

    public void CreateFactory()
    {
        CreateBuilding(factoryPrefab, 50);
    }

    public bool TakeMoney(float cost)
    {
        if (money < cost)
        {
            return false;
        }
        money -= cost;
        return true;
    }

    public void GiveMoney(float amount)
    {
        money += amount;
    }
}
