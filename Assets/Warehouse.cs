﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warehouse : Building {
    
    public Dictionary<Ore.Type, int> max = new Dictionary<Ore.Type, int>();
    public int currentLevel = -1;

    void Start()
    {
        gameObject.name = "Warehouse";
        items = new List<Item>();
        items.Add(new Item(Ore.Type.COAL, 0, false, 0));
        items.Add(new Item(Ore.Type.COPPER, 0, false, 0));
        items.Add(new Item(Ore.Type.IRON, 0, false, 0));
        items.Add(new Item(Ore.Type.ROCKETPART, 0, false, 0));
        SetLevel(0);
    }

    public void Add(Ore.Type type, int amount)
    {
        foreach (Item item in items)
        {
            if (item.name == type)
            {
                item.amount += amount;
                if (item.amount > item.capacity)
                    item.amount = item.capacity;
            }
        }
    }

    public float Get(Ore.Type type)
    {
        foreach(Item item in items)
        {
            if (item.name == type)
            {
                return item.amount;
            }
        }
        return 0;
    }

    public void Set(Ore.Type type, float amount)
    {
        foreach (Item item in items)
        {
            if (item.name == type)
            {
                item.amount = amount;
            }
        }
    }

    public void SetLevel(int level)
    {
        currentLevel = level;
        if (level == 3)
        {
            max[Ore.Type.COAL] = 500;
            max[Ore.Type.COPPER] = 500;
            max[Ore.Type.IRON] = 500;
            max[Ore.Type.ROCKETPART] = 100;
        }
        if (level == 2)
        {
            max[Ore.Type.COAL] = 300;
            max[Ore.Type.COPPER] = 150;
            max[Ore.Type.IRON] = 100;
            max[Ore.Type.ROCKETPART] = 50;
        }
        if (level == 1)
        {
            max[Ore.Type.COAL] = 100;
            max[Ore.Type.COPPER] = 50;
            max[Ore.Type.IRON] = 20;
            max[Ore.Type.ROCKETPART] = 0;
        }
        if (level == 0)
        {
            max[Ore.Type.COAL] = 100;
            max[Ore.Type.COPPER] = 0;
            max[Ore.Type.IRON] = 0;
            max[Ore.Type.ROCKETPART] = 0;

        }

        foreach(Item item in items)
        {
            item.capacity = max[item.name];
        }
    }

    void Update()
    {
        string text = "";
        foreach (Item item in items)
        {
            if (text != null)
                text += "\n";

            text += item.ToString();
        }
        GetComponentInParent<ObjectWithUI>().Text = text;
    }

    public void Upgrade()
    {
        int cost = 999999999;
        switch(currentLevel)
        {
            case 0: cost = 20; break;
            case 1: cost = 50; break;
            case 2: cost = 100; break;
        }
        if (!Headquarters.Instance.TakeMoney(cost))
        {
            StatusText.Instance.Text = "You don't have enough money, you need to have $" + cost;
            return;
        }
        SetLevel(currentLevel + 1);
    }
	public void SellEnable ()
	{
        GameObject sellUi = new GameObject();
        sellUi.name = "SellUI";
        sellUi.transform.parent = transform;
        sellUi.transform.position = GetComponent<ObjectWithUI>().uiInst.transform.position;
        ObjectWithUI sellUiScript = sellUi.AddComponent<ObjectWithUI>();
        sellUiScript.uiButtons = new ObjectWithUI.UIButtonAction[]
        {
            new ObjectWithUI.UIButtonAction("Carbon Ore", SellCoal),
            new ObjectWithUI.UIButtonAction("Copper Ore", SellCopper),
            new ObjectWithUI.UIButtonAction("Iron Ore", SellIron)
        };
        sellUiScript.buttonPrefab = GetComponent<ObjectWithUI>().buttonPrefab;
        sellUiScript.OpenUI();
    }
		
    public void Sell(Ore.Type type, int amount)
    {
        amount = (int)items.Find(R => R.name == type).amount;

        if (Get(type) < amount)
        {
            StatusText.Instance.Text = "Too low " + type + " to sell it";
            return;
        }
        float money = 0.0f;
        switch(type)
        {
            case Ore.Type.COAL: money = amount ; break;
            case Ore.Type.COPPER: money = amount * 2.0f; break;
            case Ore.Type.IRON: money = amount * 5.0f; break;
        }
        Add(type, -amount);
        Headquarters.Instance.GiveMoney(money);
        StatusText.Instance.Text = "You sold " + amount + " " + type + " for $" + amount;
    }

    public void SellCoal() { Sell(Ore.Type.COAL, 1); }
    public void SellCopper() { Sell(Ore.Type.COPPER, 1); }
    public void SellIron() { Sell(Ore.Type.IRON, 1); }

    public override void processTrain(List<Item> list)
    {
        // First, put everything you can away
        for (int i = 0; i < list.Count; i++)
        {
            Item item = items.Find(r => r.name == list[i].name);
            if (item != null)
            {
                Debug.Log(gameObject.name + ": deposit " + list[i].name);
                float x = Item.move(list[i], item);
                Debug.Log("Moved " + x + " items");
            }
        }

        // Then, take items that are needed
        for (int i = 0; i < list.Count; i++)
        {
            Item item = items.Find(r => r.name == list[i].name);
            if (item != null && list[i].isNeeded)
            {
                Debug.Log(gameObject.name + ": take " + list[i].name + " again, it's needed");
                float x = Item.move(item, list[i]);
                Debug.Log("Moved " + x + " items");
            }
        }
    }
}
