﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using HoloToolkit.Unity;
using HoloToolkit.Unity.SpatialMapping;
using System;
using HoloToolkit.Unity.InputModule;
using System.Collections;

public class PlaySpaceManager : Singleton<PlaySpaceManager>, IInputClickHandler
{
    public GameObject orePrefab;
    public GameObject greenStuff;
    public GameObject headquartersPrefab;
    public Material hideSpatialUnderstandingMaterial;
    private GameObject a;

    private void Start()
    {
        SpatialUnderstanding.Instance.OnScanDone += SpatialUnderstanding_OnScanDone;
        InputManager.Instance.OverrideFocusedObject = gameObject;
        StatusText.Instance.Text = "Terrain scanning...\nTap to finish scanning";
    }
    
    private void OnDestroy()
    {
        if (SpatialUnderstanding.Instance != null)
        {
            SpatialUnderstanding.Instance.OnScanDone -= SpatialUnderstanding_OnScanDone;
        }
        if (InputManager.Instance.OverrideFocusedObject == gameObject)
        {
            InputManager.Instance.OverrideFocusedObject = null;
        }
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {
        if (InputManager.Instance.OverrideFocusedObject == gameObject)
        {
            InputManager.Instance.OverrideFocusedObject = null;
        }

        Debug.Log("Finish scan");
        StatusText.Instance.Text = "Finish scan...";

        if (SpatialMappingManager.Instance.IsObserverRunning())
        {
            SpatialMappingManager.Instance.StopObserver();
        }

        SpatialUnderstanding.Instance.RequestFinishScan();
    }

    private void SpatialUnderstanding_OnScanDone()
    {
        StatusText.Instance.Text = "Tap to place the headquarters";
        GameObject.Instantiate(headquartersPrefab);
    }

    private bool generatingObjects = false;
    private List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult> generatingObjectsList = null;

    private void Update()
    {
        // Po umiejscowieniu blokujemy mozliwosc przenoszenia
        if (Headquarters.Instance != null)
        {
            TapToPlace tapToPlace = Headquarters.Instance.gameObject.GetComponent<TapToPlace>();
            if (tapToPlace != null && tapToPlace.enabled)
            {
                if (!tapToPlace.IsBeingPlaced)
                {
                    tapToPlace.enabled = false;

                    StatusText.Instance.Text = "Generating objects...";
                    generatingObjects = true;
#if UNITY_EDITOR || !UNITY_WSA
                    new System.Threading.Thread((() => GenerateObjects() )).Start();
#else
                    System.Threading.Tasks.Task.Run((() => GenerateObjects() ));
#endif
                }
            }

            if (!generatingObjects && generatingObjectsList != null)
            {

                foreach (SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult result in generatingObjectsList)
                {
                    GameObject.Instantiate(orePrefab, result.Position, Quaternion.identity);
                    a = GameObject.Instantiate(greenStuff, result.Position + new Vector3(UnityEngine.Random.Range(-0.02f, -0.68f), 0, UnityEngine.Random.Range(0.02f, 0.68f)), Quaternion.identity);
                    a.tag = "greenStuff";
                    a = GameObject.Instantiate(greenStuff, result.Position + new Vector3(UnityEngine.Random.Range(-0.02f, -0.68f), 0, UnityEngine.Random.Range(-0.02f, -0.68f)), Quaternion.identity);
                    a.tag = "greenStuff";
                }
                generatingObjectsList = null;
                StatusText.Instance.Text = "Scanning accomplished!";
                SpatialUnderstanding.Instance.UnderstandingCustomMesh.MeshMaterial = hideSpatialUnderstandingMaterial;
            }
        }
    }

    private void GenerateObjects()
    {
        generatingObjectsList = new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult>();

        SpatialUnderstandingDllObjectPlacement.Solver_Init();

        List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule> rules = new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule>() {
            SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromWalls(0.8f),
            SpatialUnderstandingDllObjectPlacement.ObjectPlacementRule.Create_AwayFromOtherObjects(1.5f),
        };

        List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementConstraint> constraints = new List<SpatialUnderstandingDllObjectPlacement.ObjectPlacementConstraint> {
            SpatialUnderstandingDllObjectPlacement.ObjectPlacementConstraint.Create_AwayFromWalls(),
            SpatialUnderstandingDllObjectPlacement.ObjectPlacementConstraint.Create_AwayFromOtherObjects()
        };

        for (int i = 0; ; i++)
        {
            SpatialUnderstandingDllObjectPlacement.ObjectPlacementResult result = ObjectPlacement.PlaceObject(
                "OreFloor" + i,
                  SpatialUnderstandingDllObjectPlacement.ObjectPlacementDefinition.Create_OnFloor(new Vector3(0.1f, 0.01f, 0.1f)),
                rules,
                constraints);

            if (result == null) break;
            generatingObjectsList.Add(result);
        }
        
        generatingObjects = false;
    }
}