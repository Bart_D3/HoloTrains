﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleTrainWarehose {

    List<Item> curItems;
    float capacity = 50;

    public void init()
    {
        curItems = new List<Item>();
        curItems.Add(new Item(Ore.Type.COAL, 0, false, capacity));
        curItems.Add(new Item(Ore.Type.COPPER, 0, false, capacity));
        curItems.Add(new Item(Ore.Type.IRON, 0, false, capacity));
    }

    public void trainToBuilding(List<Item> list)
    {
        Debug.Log("taking from train to building" + Time.time);
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].isNeeded)
            {
                Item item = curItems.Find(r => r.name == list[i].name);
                if (item != null)
                {
                    list[i].add(item.take(list[i].capacity - list[i].amount));
                }
            }
        }
    }

    public List<Item> getItems()
    {
        return curItems;
    }
}
