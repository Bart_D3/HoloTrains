﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainMovement : MonoBehaviour, IFocusable
{
    private List<GameObject> wayPoints;
    
    

    private int index = 0;

    private TrainWarehouse warehouse;
    
    // Use this for initialization
    void Start()
    {
        warehouse = new TrainWarehouse();
        warehouse.init();
        wayPoints = TrainPathMaker.Instance.getWayPoints();
        Debug.Log("Start train");
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GazeManager.Instance.HitObject == gameObject)
        {
            StatusText.Instance.Text = "is going to point " + (index + 1) + "/" + wayPoints.Count + ": " + wayPoints[index].name;
            foreach (Item item in warehouse.getItems())
            {
                StatusText.Instance.Text += "\n"+item.ToString();
            }
            StatusText.Instance.MoveToAbove(gameObject);
        }

        //Debug.Log(index);
        if (Vector3.Distance(transform.position, wayPoints[index].transform.position) <= 0.1f)
        {
            transform.position = wayPoints[index].transform.position;
            
            Building building = wayPoints[index].GetComponentInChildren<Building>();
            if (building != null)
            {
                building.processTrain(warehouse.getItems());
            }

            //Debug.Log(building.writeItems());
            //warehouse.logItems();

            index++;
            if (index >= wayPoints.Count)
            {
                index = 0;
            }
        }


        transform.position = Vector3.MoveTowards(transform.position, wayPoints[index].transform.position, 0.5f * Time.deltaTime);
        transform.LookAt(wayPoints[index].transform.position);
    }

    /*private void OnTriggerEnter(Collider collider)
    {
        //Debug.Log("Collision "+index);

        if (wayPoints != null)
        {
            transform.position = wayPoints[index].transform.position;
            if (collider.gameObject.transform.parent.gameObject != wayPoints[index]) return;

            warehouse.takeWhatYouNeed(collider.transform.parent.GetComponent<Building>().getItems());
            collider.transform.parent.GetComponent<Building>().takeWhatYouNeed(warehouse.getItems());
            warehouse.orderItems(collider.transform.parent.GetComponent<Building>().getItems());

            Debug.Log(collider.transform.parent.GetComponent<Building>().writeItems());
            warehouse.logItems();
        }
        
        index++;
        if (wayPoints != null && index >= wayPoints.Count)
        {
            index = 0;
        }
    }*/

    public void OnFocusEnter()
    {
        StatusText.Instance.Text = "Pociąg";
        StatusText.Instance.MoveToAbove(gameObject);
    }

    public void OnFocusExit()
    {
        StatusText.Instance.Text = "...";
        //audioSource.Pause();
    }
}

