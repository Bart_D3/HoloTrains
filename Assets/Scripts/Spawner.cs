﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Spawner : MonoBehaviour, IInputClickHandler
{

    public GameObject place;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        Debug.Log("Clicked");
        gameObject.AddComponent<Rigidbody>();

        place = GameObject.FindGameObjectWithTag("Spawner");
        place.GetComponent<TrainPathMaker>().addWayPoint(this.gameObject);
    }
}
