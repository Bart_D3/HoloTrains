﻿using HoloToolkit.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : Singleton<Manager>
{

    private List<GameObject> connections = new List<GameObject>();
    private List<GameObject> waypoints = new List<GameObject>();

    private List<GameObject> buildings = new List<GameObject>();

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void addConnection(GameObject con)
    {
        connections.Add(con);
    }

    public void removeConnection(GameObject item)
    {
        //var itemToRemove = resultlist.Single(r => r.Id == 2);
        connections.Remove(item);
    }

    public void GetConnectionById()
    {
        //return connections.Find(r => r.GetComponent<ID>().id);

    }

    public void addWaypoint(GameObject way)
    {
        waypoints.Add(way);
    }

    public void removeWaypoint(GameObject item)
    {
        //var itemToRemove = resultlist.Single(r => r.Id == 2);
        waypoints.Remove(item);
    }

    public void GetWayPointById()
    {
        //return waypoints.Find(r => r.GetComponent<ID>().id);

    }

    public GameObject isConnection(GameObject p1,GameObject p2)
    {
        GameObject isCon=null;
        for(int i = 0; i < connections.Count; i++)
        {
            if (connections[i].GetComponent<PieceOfRoad>().hasPoints(p1, p2))
            {
                isCon= connections[i];
            }
            
        }
        return isCon;
    }

    public GameObject[] getWaypointsConnections(GameObject waypoint)
    {
        List<GameObject> con = new List<GameObject>();
        for(int i = 0; i < connections.Count; i++)
        {
            if (connections[i].GetComponent<PieceOfRoad>().hasPoint(waypoint)) {
                con.Add(connections[i]);
            }
        }
        return con.ToArray();

        
    }
}
