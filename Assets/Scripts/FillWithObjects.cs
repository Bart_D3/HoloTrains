﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillWithObjects : MonoBehaviour
{

    public GameObject one;
    public GameObject two;

    public GameObject fillPrefab;
    // Use this for initialization

    public float distance;
    public float height;
    public float rest;
    public int amount;
    public float scale;

    public List<GameObject> list;
    void Start()
    {
        list = new List<GameObject>();
        fill();
    }

    // Update is called once per frame
    void Update()
    {
        
            clearList();
            fill();
        
    }

    public void fill()
    {
        height = fillPrefab.GetComponent<CapsuleCollider>().height;
        distance = Vector3.Distance(one.transform.position, two.transform.position);

        amount = (int)(distance / height);
        rest = distance - (amount * height);
        scale = (height + (rest / amount)) / height;
        for (int i = 0; i < amount; i++)
        {
            GameObject g = Instantiate(fillPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            g.transform.position = (one.transform.position + (two.transform.position - one.transform.position) / amount * i) + (two.transform.position - one.transform.position) / (amount * 2);
            g.transform.rotation = Quaternion.LookRotation(two.transform.position - one.transform.position);
            g.transform.Rotate(90.0f, g.transform.rotation.y, g.transform.rotation.z);
            g.transform.localScale = new Vector3(g.transform.localScale.x, scale, g.transform.localScale.z);
            list.Add(g);
        }

    }
    public void clearList()
    {
        for (int i = 0; i < list.Count; i++)
        {
            Destroy(list[i]);

        }
        list.Clear();
    }
}
