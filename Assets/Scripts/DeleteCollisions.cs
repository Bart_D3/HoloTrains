﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteCollisions : MonoBehaviour {

    GameObject[] obj;
    

    void OnCollisionStay(Collision collision)
    {
        
            if (collision.gameObject.tag == "greenStuff")
            {
                DestroyImmediate(collision.gameObject);
            }
    }

}
