﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceOfRoad : MonoBehaviour {

    private GameObject point1;
    private GameObject point2;

    private LineRenderer line;
    private CapsuleCollider capsule;

    public GameObject fillPrefab;

    private float lineWidth = 10f;


    public List<GameObject> list;

    public bool isMoving;

    private List<GameObject> trains;

    // Use this for initialization
    void Start () {
        isMoving = false;
        list = new List<GameObject>();
        line = GetComponent<LineRenderer>();
        line.SetWidth(0.1f, 0.25f);

        capsule = gameObject.AddComponent<CapsuleCollider>()as CapsuleCollider;
        capsule.radius = lineWidth / 2;
        capsule.center = Vector3.zero;
        capsule.direction = 50;
        trains = new List<GameObject>();
    }

    
	
	// Update is called once per frame
	void Update () {
        
        capsule.transform.position = point1.transform.position + (point2.transform.position - point1.transform.position) / 2;
        capsule.transform.LookAt(point1.transform.position);
        capsule.height = (point2.transform.position - point1.transform.position).magnitude;
        if (isMoving)
        {
            clearList();
            fill();
        }
    }

    public void setPoints(GameObject p1, GameObject p2)
    {
        point1 = p1;
        point2 = p2;
        clearList();
        fill();
    }
    public void drawLine()
    {
        line.SetPosition(0, point1.transform.position);
        line.SetPosition(1, point2.transform.position);
    }

    public bool hasPoints(GameObject p1,GameObject p2)
    {
        return (p1 == point1 || p1 == point2) && (p2 == point2 || p2 == point1);
    }
    public bool hasPoint(GameObject p)
    {
        return p== point1 || p == point2;
    }

    public void fill()
    {
        float height = fillPrefab.GetComponent<CapsuleCollider>().height;
        float distance = Vector3.Distance(point1.transform.position, point2.transform.position);

        int amount = (int)(distance / height);
        float rest = distance - (amount * height);
        float scale = (height + (rest / amount)) / height;
        for (int i = 0; i < amount; i++)
        {
            GameObject g = Instantiate(fillPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            g.transform.position = (point1.transform.position + (point2.transform.position - point1.transform.position) / amount * i) + (point2.transform.position - point1.transform.position) / (amount * 2);
            g.transform.rotation = Quaternion.LookRotation(point2.transform.position - point1.transform.position);
            g.transform.Rotate(90.0f, g.transform.rotation.y, g.transform.rotation.z);
            g.transform.localScale = new Vector3(g.transform.localScale.x, scale, g.transform.localScale.z);
            list.Add(g);
        }

    }

    public void clearList()
    {
        for (int i = 0; i < list.Count; i++)
        {
            Destroy(list[i]);

        }
        list.Clear();
    }

    public void delete()
    {
        Manager.Instance.removeConnection(gameObject);
        for(int i = 0; i < trains.Count; i++)
        {
            Destroy(trains[i]);
        }
        Destroy(gameObject);
    }


    public void addTrain(GameObject train)
    {
        trains.Add(train);
    }
}
