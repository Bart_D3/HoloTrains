﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item  {

    public Item(Ore.Type name,float amount,bool isNeeded,float capacity)
    {
        this.name = name;
        this.amount = amount;
        this.isNeeded = isNeeded;
        this.capacity = capacity;
    }

    public Ore.Type name;
    public float amount;
    public float capacity;
    public bool isNeeded;

    // zwraca nadmiar po dodaniu
    public float add(float i)
    {
        amount += i;
        if (amount > capacity)
        {
            float rest = amount - capacity;
            amount = capacity;
            return rest;
        }
        else
        {
            return 0;
        }
    }

    // zwraca zabrana ilosc
    public float take(float i)
    {
        if (i > amount)
        {
            float a = amount;
            amount = 0;
            return a;
        }
        amount = amount - i;
        return i;
    }

    public override string ToString()
    {
        return name+": " + amount + " / " + capacity;
    }

    // zwraca przeniesiona ilosc
    public static float move(Item from, Item to)
    {
        if (from.name != to.name)
            throw new ArgumentException();

        float to_move = Math.Min(from.amount, to.capacity - to.amount);
        from.amount -= to_move;
        to.amount += to_move;

        return to_move;
    }

}
