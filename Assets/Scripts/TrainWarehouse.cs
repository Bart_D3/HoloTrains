﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainWarehouse
{
    List<Item> curItems;
    private const float capacity = 50;

    public void init()
    {
        curItems = new List<Item>();
        curItems.Add(new Item(Ore.Type.COAL, 0, false, capacity));
        curItems.Add(new Item(Ore.Type.COPPER, 0, false, capacity));
        curItems.Add(new Item(Ore.Type.IRON, 0, false, capacity));
        curItems.Add(new Item(Ore.Type.ROCKETPART, 0, false, capacity));
    }

    public List<Item> getItems()
    {
        return curItems;
    }
}
