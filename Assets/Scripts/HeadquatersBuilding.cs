﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadquatersBuilding : Building {

    float capacityIron = 200.0f;
    float capacityCopper = 100.0f;
    float capacityRocketParts = 50.0f;


    public GameObject rocket;
    void Start()
    {
        rocket.SetActive(false);
        items = new List<Item>();
        items.Add(new Item(Ore.Type.IRON, 0, true, capacityIron));
        items.Add(new Item(Ore.Type.COPPER, 0, true, capacityCopper));
        items.Add(new Item(Ore.Type.ROCKETPART, 0, true, capacityCopper));
        // Use this for initialization


    }

    // Update is called once per frame
    void Update() {
        if (items.Find(r => r.name == Ore.Type.IRON).amount >= capacityIron && items.Find(r => r.name == Ore.Type.COPPER).amount >= capacityCopper&& items.Find(r => r.name == Ore.Type.ROCKETPART).amount >= capacityRocketParts)
        {
            rocket.SetActive(true);
            rocket.GetComponent<Rocket>().isFlying = true;
        }
    }

    public override void processTrain(List<Item> list)
    {
        // Deposit all the required items
        for (int i = 0; i < list.Count; i++)
        {
            Item item = items.Find(r => r.name == list[i].name);
            if (item != null && item.isNeeded)
            {
                list[i].isNeeded = true;
                Debug.Log(gameObject.name + ": deposit " + list[i].name + " to Headquarters");
                float x = Item.move(list[i], item);
                Debug.Log("Moved " + x + " items");
            }
        }
    }
}
