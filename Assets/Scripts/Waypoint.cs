﻿using HoloToolkit.Unity.InputModule;
using HoloToolkit.Unity;
using HoloToolkit.Unity.SpatialMapping;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(TapToPlace))]
public class Waypoint : MonoBehaviour {

    public void manipulationStart()
    {
        WaypointMover.Instance.startManipulationAt(gameObject);
    }


    public void tabToPlace()
    {

    }
    
}
