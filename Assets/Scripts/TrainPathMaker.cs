﻿using HoloToolkit.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;

public class TrainPathMaker : Singleton<TrainPathMaker>
{
    private List<GameObject> wayPoints;
    private LineRenderer line;
    private GameObject startWaypoint;
    private GameObject currentWaypoint;
    

    public GameObject[] trainPrefabs;
    
    // Use this for initialization
    void Start()
    {


        wayPoints = new List<GameObject>();
        line = GetComponent<LineRenderer>();
        line.SetWidth(0.1f, 0.1f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool addWayPoint(GameObject wP)
    {
        
            if (wayPoints.Count < 1 || Manager.Instance.isConnection(wayPoints[wayPoints.Count - 1], wP) != null)
            {
                if (wayPoints.Count > 0 && wP == wayPoints[0])
                {
                    if (wayPoints.Count > 1)
                    {
                        spawnTrain();
                        return false;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    if (Headquarters.Instance.TakeMoney(10))
                    {
                        wayPoints.Add(wP);
                        drawTrainPath();
                    return true;
                    }
                    else
                    {
                        StatusText.Instance.Text = "Not enough Money";
                    return false;
                    }
                }
                return true;
            }
            return true;
    }
    

    public void endDrawingPath()
    {
        wayPoints.Clear();drawTrainPath();

        

    }

    public void drawTrainPath()
    {
        Vector3[] trans = new Vector3[wayPoints.Count];
        for (int i = 0; i < wayPoints.Count; i++)
        {
            trans[i] = wayPoints[i].transform.position;
        }
        line.SetVertexCount(trans.Length);
        line.SetPositions(trans);

    }


    public void spawnTrain()
    {
        System.Random rand = new System.Random();
        GameObject train = Instantiate(trainPrefabs[rand.Next(0,trainPrefabs.Length)], wayPoints[0].transform.position, Quaternion.identity);
        train.transform.localScale=new Vector3(3.0f,3.0f,3.0f);

        for(int i = 0; i+1 < wayPoints.Count; i++)
        {
            GameObject con =Manager.Instance.isConnection(wayPoints[i], wayPoints[i + 1]);
            if (con != null)
            {
                con.GetComponent<PieceOfRoad>().addTrain(train);
            }
        }



    }

    public List<GameObject> getWayPoints()
    {
        List<GameObject> list = new List<GameObject>();
        for (int i = 0; i < wayPoints.Count; i++)
        {
            list.Add(wayPoints[i]);
        }
        endDrawingPath();
        return list;
    }
}
