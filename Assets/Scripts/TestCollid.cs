﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCollid : MonoBehaviour {
    public GameObject pathMaker;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        pathMaker.GetComponent<TrainPathMaker>().addWayPoint(this.gameObject);
    }
    
}
