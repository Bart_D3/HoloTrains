﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Building : MonoBehaviour {

    protected List<Item> items;

    public abstract void processTrain(List<Item> list);

    public List<Item> getItems()
    {
        return items;
    }
}
