﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestInput : MonoBehaviour {

    public KeyCode key; 
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(key))
        {
            GameObject.FindGameObjectWithTag("TrainPathMaker").GetComponent<TrainPathMaker>().addWayPoint(this.gameObject);
        }
    }
}
