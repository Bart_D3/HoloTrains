﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class UIButton : MonoBehaviour, IInputClickHandler, IFocusable {
    public UnityEvent action;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        GetComponentInParent<ObjectWithUI>().CloseUI();
        action.Invoke();
    }

    public void OnFocusEnter()
    {
        StatusText.Instance.Text = gameObject.name;
        StatusText.Instance.MoveToAbove(gameObject);
    }

    public void OnFocusExit()
    {
        StatusText.Instance.Text = "...";
    }
}
