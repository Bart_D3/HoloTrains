﻿using HoloToolkit.Unity;
using HoloToolkit.Unity.SpatialMapping;
using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RoadPoint : MonoBehaviour, IInputClickHandler {
    public void StartNewTrackHere()
    {
        RoadPlanner.Instance.StartNewTrackAt(gameObject);
    }

    public static bool isPlanning = false;

    public void StartTrainPlanning()
    {
        isPlanning = true;
        StatusText.Instance.Text = "The way is generating from" + gameObject.name;
        TrainPathMaker.Instance.addWayPoint(gameObject);
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {
        if (!isPlanning)
        {
            return;
        }

        StatusText.Instance.Text = "Added " + gameObject.name + " to the way";

        isPlanning = TrainPathMaker.Instance.addWayPoint(gameObject);

        if (!isPlanning)
        {
            StatusText.Instance.Text = "The way was created";
        }
    }
}
