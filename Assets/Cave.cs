﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cave : Building
{

    public Ore.Type oreType;
    private int nextUpdate = 1;
    private float capacity = 50;

    // Use this for initialization
    void Start ()
    {
        items = new List<Item>();
        items.Add(new Item(oreType, 20, false, capacity));
        if (oreType != Ore.Type.COAL)
            items.Add(new Item(Ore.Type.COAL, 20, true, capacity));
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Time.time >= nextUpdate)
        {
            nextUpdate = Mathf.FloorToInt(Time.time) + 1;

            if (oreType == Ore.Type.COAL)
            {
                items.Find(r => r.name == Ore.Type.COAL).add(1);
            }
            else
            {
                if (items.Find(r => r.name == Ore.Type.COAL).take(1) == 1)
                {
                    if (items.Find(r => r.name == oreType).add(1) != 0)
                    {
                        items.Find(r => r.name == Ore.Type.COAL).add(1);
                    }
                }
            }
        }

        string text = "";
        foreach (Item item in items)
        {
            if (text != null)
                text += "\n";

            text += item.ToString();
        }
        GetComponentInParent<ObjectWithUI>().Text = text;
    }

    public override void processTrain(List<Item> list)
    {
        // First, take items that are needed
        for (int i = 0; i < list.Count; i++)
        {
            Item item = items.Find(r => r.name == list[i].name);
            if (item != null && item.isNeeded)
            {
                // Make sure the train knows we need this item
                list[i].isNeeded = true;

                Debug.Log(gameObject.name + ": " + list[i].name + " is needed, so take it");
                float x = Item.move(list[i], item);
                Debug.Log("Moved " + x + " items");
            }
        }

        // Then, put everything that is not needed away
        for (int i = 0; i < list.Count; i++)
        {
            Item item = items.Find(r => r.name == list[i].name);
            if (item != null && !item.isNeeded)
            {
                Debug.Log(gameObject.name + ": " + list[i].name + " is not needed, so put it away");
                float x = Item.move(item, list[i]);
                Debug.Log("Moved " + x + " items");
            }
        }
    }
}
