﻿using HoloToolkit.Unity.InputModule;
using HoloToolkit.Unity;
using HoloToolkit.Unity.SpatialMapping;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(TapToPlace))]
public class WaypointMover : Singleton<WaypointMover>, IInputClickHandler
{
    public bool isBUilded = false;
    public bool isInEddition = false;
    GameObject[] connections;
    private Vector3 startPosition;
    GameObject wayPoint;

    public void startManipulationAt(GameObject wP)
    {
        wayPoint=wP;
        connections = Manager.Instance.getWaypointsConnections(gameObject);
        for (int i = 0; i < connections.Length; i++)
        {
            connections[i].GetComponent<PieceOfRoad>().isMoving = true;
        }
        isInEddition = true;
        startPosition = transform.position;
        InputManager.Instance.OverrideFocusedObject = gameObject;
    }

    public void onManipulationEnd()
    {
        for (int i = 0; i < connections.Length; i++)
        {
            connections[i].GetComponent<PieceOfRoad>().isMoving = false;
        }
        isInEddition = false;
        wayPoint = null;
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {
        if (isBUilded)
        {
            if (isInEddition)
            {
                wayPoint.transform.position = GazeManager.Instance.HitPosition;
                onManipulationEnd();
                
            }
        }
    }

    public void Update()
    {
        if (isInEddition)
        {

            wayPoint.transform.position = GazeManager.Instance.HitPosition;

        }
    }
}
