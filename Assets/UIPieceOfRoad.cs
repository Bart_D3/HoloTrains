﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using HoloToolkit.Unity;
using UnityEngine.Events;

public class UIPieceOfRoad : MonoBehaviour
{
	public static UIPieceOfRoad CurrentlyOpenUI { get; private set; }

	public GameObject buttonPrefab, Piece;
	public string Text { get; set; }
	public GameObject buttonInst;

	public void OnInputClicked(InputClickedEventData eventData)
	{
		if (CurrentlyOpenUI == this)
		{
			CloseUI();
		}
		else
		{
			OpenUI();
		}
	}

	public GameObject uiInst;
	/*void OnMouseDown(GameObject source)
	{
		if (k == 1) {
			GameObject.Find ("PieceOfRoad").GetComponent<PieceOfRoad> ().delete ();
		} else if (k == 2) {
			CloseUI ();
		}
		return;
	}*/
	public void OpenUI()
	{
		if (CurrentlyOpenUI != null) 
		{
			CurrentlyOpenUI.CloseUI ();
		}

		CurrentlyOpenUI = this;

		for (int i = 0; i < 2; i++) 
		{
			buttonInst = GameObject.Instantiate (buttonPrefab);
			buttonInst.transform.localPosition = new Vector3 (-0.25f / 2 + i * 0.25f, 0.0f, 0.0f);
			UIButton buttonScript = buttonInst.AddComponent<UIButton> ();
			if (i == 0){
				buttonInst.GetComponent<Renderer> ().material.color = Color.yellow;
				buttonInst.name = "Remove railways";
			}
			else{
				buttonInst.GetComponent<Renderer> ().material.color = Color.red;
				buttonInst.name = "Close";
			}
		}
	

	
		if (CurrentlyOpenUI == this)
			return;

		if (CurrentlyOpenUI != null)
		{
			CurrentlyOpenUI.CloseUI();
		}
		CurrentlyOpenUI = this;
	}
	public void CloseUI()
	{
		if (CurrentlyOpenUI != this)
			return;
		GameObject.Destroy(uiInst);
		CurrentlyOpenUI = null;
	}
	public void OnFocusEnter()
	{
		StatusText.Instance.Text = gameObject.name + (Text != null && Text != "" ? "\n" + Text : "");		StatusText.Instance.MoveToAbove(gameObject);
	}

	public void OnFocusExit()
	{
		StatusText.Instance.Text = "...";
	}
}
