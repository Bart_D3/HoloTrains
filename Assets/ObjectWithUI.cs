﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using HoloToolkit.Unity;
using UnityEngine.Events;

public class ObjectWithUI : MonoBehaviour, IInputClickHandler, IFocusable
{
	public static ObjectWithUI CurrentlyOpenUI { get; private set; }

	[System.Serializable]
	public struct UIButtonAction
	{
		public string buttonName;
		public UnityEvent buttonAction;

        public UIButtonAction(string name, UnityAction action) : this()
        {
            buttonName = name;
            buttonAction = new UnityEvent();
            buttonAction.AddListener(action);
        }
    }

	public UIButtonAction[] uiButtons;
	public GameObject buttonPrefab;
	public string Text { get; set; }

	public void OnInputClicked(InputClickedEventData eventData)
	{
		if (RoadPoint.isPlanning)
			return;

		if (CurrentlyOpenUI == this)
		{
			CloseUI();
		}
		else
		{
			OpenUI();
		}
	}

	public GameObject uiInst;

	public void OpenUI()
	{
		if (CurrentlyOpenUI == this)
			return;

		if (CurrentlyOpenUI != null)
		{
			CurrentlyOpenUI.CloseUI();
		}
		CurrentlyOpenUI = this;


		uiInst = new GameObject();
		uiInst.name = "UI";
		uiInst.transform.parent = this.transform;
		uiInst.transform.localPosition = new Vector3(0.0f, 0.35f / this.transform.localScale.y, 0.0f);
		Billboard billboard = uiInst.AddComponent<Billboard>();
		billboard.PivotAxis = PivotAxis.Y;

        uiInst = new GameObject();
        uiInst.name = "UI";
        uiInst.transform.parent = this.transform;
        Bounds combinedBounds = new Bounds(transform.position, Vector3.zero);
        foreach (Renderer render in GetComponentsInChildren<Renderer>())
        {
            if (render.GetComponentInParent<UIButton>() != null)
                continue;
            if (render.bounds.extents == Vector3.zero)
                continue;
            combinedBounds.Encapsulate(render.bounds);
        }
        uiInst.transform.position = new Vector3(
            combinedBounds.center.x,
            combinedBounds.max.y + 0.2f,
			combinedBounds.center.z);
        
        billboard = uiInst.AddComponent<Billboard>();
        billboard.PivotAxis = PivotAxis.Y;

		for (int i = 0; i < uiButtons.Length; i++)
		{
			GameObject buttonInst = GameObject.Instantiate(buttonPrefab);
			buttonInst.name = uiButtons[i].buttonName;
			buttonInst.transform.parent = uiInst.transform;
			buttonInst.transform.localPosition = new Vector3(-0.25f * (uiButtons.Length-1) / 2 + i*0.25f, 0.0f, 0.0f);
			UIButton buttonScript = buttonInst.AddComponent<UIButton>();
			buttonScript.action = uiButtons[i].buttonAction;
			if (i == 0)
				buttonInst.GetComponent<Renderer> ().material.color = Color.gray;
			else if (i == 1 && uiButtons.Length>2)
				buttonInst.GetComponent<Renderer> ().material.color = Color.blue;
			else if (i==1 && uiButtons.Length==2)
				buttonInst.GetComponent<Renderer> ().material.color = Color.red;
			else if (i == 2 && uiButtons.Length>3)
				buttonInst.GetComponent<Renderer> ().material.color = Color.yellow;
			else if (i==2 && uiButtons.Length==3) 
				buttonInst.GetComponent<Renderer> ().material.color = Color.red;
			else if (i == 3 && uiButtons.Length == 4) {
				buttonInst.GetComponent<Renderer> ().material.color = Color.red;
			} else if (i == 4) {
				buttonInst.GetComponent<Renderer> ().material.color = Color.red;
			}
			else buttonInst.GetComponent<Renderer> ().material.color = Color.cyan;
		}
	}
		
	public void CloseUI()
	{
		if (CurrentlyOpenUI != this)
			return;
		GameObject.Destroy(uiInst);
		CurrentlyOpenUI = null;
	}
    public void OnFocusEnter()
    {
        StatusText.Instance.Text = gameObject.name + (Text != null && Text != "" ? "\n" + Text : "");
        StatusText.Instance.MoveToAbove(gameObject);
    }

	public void OnFocusExit()
	{
		StatusText.Instance.Text = "...";
	}
}